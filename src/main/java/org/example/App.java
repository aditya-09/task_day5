package org.example;

import java.util.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String [] args){

        String[] arr1 = {"a", "b", "c"};
        String[] arr2 = {"c", "d"};

//        Alphabet 1
        ArrayList<String> alpha1 = new ArrayList<>(Arrays.asList(arr1));

//        Alphabet 2
        ArrayList<String> alpha2 = new ArrayList<>(Arrays.asList(arr2));

//        convert Alphabet
        ArrayList<String> cAlpha = new ArrayList<>(alpha1);

//        3 Lines
        cAlpha.retainAll(alpha2);
        alpha1.addAll(alpha2);
        alpha1.removeAll(cAlpha);

//        Output alpha1
        System.out.println(alpha1);

    }
}
